import os
import pipes
import subprocess
from glob import glob

from prefect import task


def chek_if_exist(ssh_host, path):
    """Функция проверит существует ли path на удаленном сервере ssh_host.
    Для корректной работы нужно, чтобы была возможность входа на удаленный
    сервер без ввода пароля. (Должны быть настроены ssh-ключи).

    Parameters
    ----------
    ssh_host : str
        название удаленного сервера его ip или host.
    path : str
        путь к файлу или папке существование чье существование мы хотим проверить.

    Returns
    -------
    bool
        если path существует, возвращаем True. В противном случае False.
    """
    resp = subprocess.call(["ssh", ssh_host, "test -e " + pipes.quote(path)])

    return not (bool(resp))


@task(log_prints=True)
def create_datetime_txt_file(cur_time, path2folder):
    data_txt_path = os.path.join(path2folder, f"{cur_time}_datetime.txt")
    cmd_create_data_txt = f"touch {data_txt_path}"
    sys_code = os.system(cmd_create_data_txt)
    if sys_code:
        raise ValueError


@task(log_prints=True)
def del_datetime_txt_files(path2folder):
    search_mask = os.path.join(path2folder, f"*_datetime.txt")
    datetime_txt_files = glob(search_mask)

    for datetime_txt_file in datetime_txt_files:
        os.remove(datetime_txt_file)


@task(log_prints=True)
def download_folder(sync_folder, remote_server, server_folder):
    print("Начинаем загрузку данных из удаленного хранилища")
    rsync_cmd = (
        f"rsync "
        f"-avzx "
        f"--no-perms "
        f"--progress "
        f"{remote_server}:{server_folder} "
        f"{sync_folder}"
    )

    sys_code = os.system(rsync_cmd)
    if sys_code:
        raise ValueError

    print("Загрузка данных из удаленного хранилища завершена")


@task(log_prints=True)
def download_folder_and_clean_remote(
    sync_folder, remote_server, server_folder, exclude=""
):
    print("Начинаем загрузку данных из удаленного хранилища")
    print("После синхронизации файлы будут удалены из удаленного хранилища")

    if exclude:
        exclude_cmd = f"--exclude '{exclude}' "

    rsync_cmd = (
        f"rsync "
        f"-avzx "
        f"--no-perms "
        f"--progress {exclude_cmd} "
        f"--remove-source-files "
        f"{remote_server}:{server_folder} "
        f"{sync_folder}"
    )

    sys_code = os.system(rsync_cmd)
    if sys_code:
        raise ValueError

    print("Загрузка данных из удаленного хранилища завершена")


@task(log_prints=True)
def upload_backup(backup_path, remote_server, server_folder, remove_src_files=True):
    print("Начинаем загрузку бекапа в сетевое хранилище")
    rsync_cmd = (
        f"rsync "
        f"-avzx "
        f"--no-perms "
        f"--progress "
        f"{backup_path} "
        f"{remote_server}:{server_folder}"
    )

    sys_code = os.system(rsync_cmd)
    if sys_code:
        raise ValueError

    print("Загрузка бекапа в сетевое хранилище завершена")

    print("Удаляю предыдущие бекапы из сетевого хранилища")
    rsync_cmd = (
        f"rsync "
        f"-avzx "
        f"--delete "
        f"--no-perms "
        f"--progress "
    )
    
    if remove_src_files:
        rsync_cmd += f"--remove-source-files "
        
    rsync_cmd += f"{backup_path} {remote_server}:{server_folder}"

    sys_code = os.system(rsync_cmd)
    if sys_code:
        raise ValueError


@task(log_prints=True)
def download_backup(server_folder, remote_server, backup_path):
    print("Начинаем скачивание бекапа из сетевого хранилища")
    rsync_cmd = (
        f"rsync "
        f"-avzx "
        f"--no-perms "
        f"--progress "
        f"{remote_server}:{server_folder} "
        f"{backup_path}"
    )

    sys_code = os.system(rsync_cmd)
    if sys_code:
        raise ValueError


@task(log_prints=True)
def print_backup_files(cvat_backup_path):
    print("Файлы бекапа созданы:")

    cmd = f"ls {cvat_backup_path}"
    sys_code = os.system(cmd)
    if sys_code:
        raise ValueError


@task(log_prints=True)
def backup_clean(cvat_backup_path, remote_server=None, files_only=True):
    cmd = f"rm -rf {cvat_backup_path}"
    
    if files_only:
        cmd += "/*"
    
    if remote_server:
        cmd = f"ssh {remote_server} {cmd}"
    
    sys_code = os.system(cmd)
    if sys_code:
        raise ValueError


@task(log_prints=True)
def copy_folder(source, destination, remote_server=None):
    cmd = f"cp -rva {source} {destination}"

    if remote_server:
        cmd = f"ssh {remote_server} {cmd}"

    sys_code = os.system(cmd)
    if sys_code:
        raise ValueError
